\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Select sequences}{2}
\contentsline {subsection}{\numberline {2.1}Reading the fasta files and select sequences (genomes with both genes)}{2}
\contentsline {subsection}{\numberline {2.2}Rename sequences with genome names}{3}
\contentsline {subsection}{\numberline {2.3}Remove true duplicates}{4}
\contentsline {subsection}{\numberline {2.4}Make concatenate}{4}
\contentsline {subsection}{\numberline {2.5}Deduplicate concatenate}{4}
\contentsline {subsection}{\numberline {2.6}Filter gene alignments}{5}
\contentsline {subsection}{\numberline {2.7}Manual alignment check}{6}
\contentsline {subsection}{\numberline {2.8}Read curated alignment, concatenate them, translate and write them}{6}
\contentsline {subsection}{\numberline {2.9}Traduction and concatenate}{7}
\contentsline {subsection}{\numberline {2.10}Phylogeny}{8}
\contentsline {subsection}{\numberline {2.11}Phylogeny}{8}
