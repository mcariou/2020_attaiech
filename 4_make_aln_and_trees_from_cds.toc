\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Select sequences}{2}
\contentsline {subsection}{\numberline {2.1}Reading the fasta files and select sequences (genomes with both genes)}{2}
\contentsline {subsection}{\numberline {2.2}Rename sequences with genome names}{5}
\contentsline {subsection}{\numberline {2.3}Remove true duplicates}{5}
\contentsline {subsection}{\numberline {2.4}Only shared sequences}{6}
\contentsline {subsection}{\numberline {2.5}Make concatenate}{6}
\contentsline {subsection}{\numberline {2.6}Deduplicate concatenate}{6}
\contentsline {subsection}{\numberline {2.7}Filter gene alignments}{8}
\contentsline {subsection}{\numberline {2.8}Manual alignment check}{9}
\contentsline {subsection}{\numberline {2.9}Read curated alignment, concatenate them, translate and write them}{9}
\contentsline {subsection}{\numberline {2.10}Traduction and concatenate}{10}
\contentsline {subsection}{\numberline {2.11}Phylogeny}{11}
\contentsline {subsection}{\numberline {2.12}Phylogeny}{12}
