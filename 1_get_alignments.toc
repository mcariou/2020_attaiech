\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {subsection}{\numberline {1.1}Background}{2}
\contentsline {subsection}{\numberline {1.2}Objective and working dir}{2}
\contentsline {section}{\numberline {2}Data: sequences of RocC and ComR gene and legionnella genomes}{2}
\contentsline {subsection}{\numberline {2.1}Origin of the genomes}{2}
\contentsline {subsection}{\numberline {2.2}Description}{3}
\contentsline {section}{\numberline {3}Search for RocC and ComR homologs in assembled genomes}{5}
\contentsline {subsection}{\numberline {3.1}Database construction}{5}
\contentsline {subsection}{\numberline {3.2}1st test and issues with database size}{5}
\contentsline {subsection}{\numberline {3.3}2nd approach and BLAST results}{6}
\contentsline {subsection}{\numberline {3.4}3rd BLAST}{6}
\contentsline {section}{\numberline {4}Blast Results}{7}
\contentsline {section}{\numberline {5}Alignements}{9}
