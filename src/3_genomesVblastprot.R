#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
if (length(args)!=5) {
  stop("5 argument must be supplied (1. table with sequences/genome caracteristics 2 and 3. blast output table 4. output).\n", call.=FALSE)
}

#args<-c("/home/mcariou/2020_Attaiech/get_alignment/ComR_sup160.tblastx", "/home/mcariou/2020_Attaiech/get_alignment/RocC_sup225.tblastx", "/home/mcariou/2020_Attaiech/get_alignment/ComEA_sup100.tblastx", "/home/mcariou/2020_Attaiech/pubgenomes/tab_ncbi_contigs_parsed.csv", "/home/mcariou/2020_Attaiech/pubgenomes/tab_ncbi_file_prot.csv") 

comr<-read.table(args[1], h=F)

names(comr)<-c("query_id", "subject_id", "perc_identity", "alignment_length", "mismatches", "gap_opens", "q.start", "q.end", "s.start", "s.end", "evalue", "bit_score")

rocc<-read.table(args[2], h=F)

names(rocc)<-c("query_id", "subject_id", "perc_identity", "alignment_length", "mismatches", "gap_opens", "q.start", "q.end", "s.start", "s.end", "evalue", "bit_score")

comea<-read.table(args[3], h=F)

names(comea)<-c("query_id", "subject_id", "perc_identity", "alignment_length", "mismatches", "gap_opens", "q.start", "q.end", "s.start", "s.end", "evalue", "bit_score")




tab<-read.table(args[4], h=T, sep=";", fill=T, comment.char = "")


tab$subject_id<-sapply(as.character(tab$fasta), function(x) strsplit(x, split="_")[[1]][1])

tab$ComR<-tab$subject_id %in% unique(comr$subject_id)

tab$RocC<-tab$subject_id %in% unique(rocc$subject_id)

tab$ComEA<-tab$subject_id %in% unique(comea$subject_id)
####
## Doesn't work, because, RocC and ComR may be on different contigs

#table(tab$ComR, tab$RocC)
 
## Count by genome/then by species

## Make a table with 1 line per genome
    
tab<-tab[order(tab$file),]

file<-unique(tab$file)
tabbyfile<-as.data.frame(file)



tabbyfile$species<-sapply(file, function(x){
    if (length(unique(tab$species[tab$file==x]))==1){
        return(unique(tab$species[tab$file==x]))
    }else{
        print(paste("inadequat file", x))
        break
    }
})

tabbyfile$size<-sapply(file, function(x){
    if (length(unique(tab$size[tab$file==x]))==1){
        return(unique(tab$size[tab$file==x]))
    }else{
        print(paste("inadequat file", x))
        break
    }
})

tabbyfile$ncontig<-sapply(file, function(x){
    if (length(unique(tab$ncontig[tab$file==x]))==1){
        return(unique(tab$ncontig[tab$file==x]))
    }else{
        print(paste("inadequat file", x))
        break
    }
})


tabbyfile$comr<-sapply(file, function(x){
    return(sum(tab$ComR[tab$file==x])>=1)
})

tabbyfile$rocc<-sapply(file, function(x){
    return(sum(tab$RocC[tab$file==x])>=1)
})

tabbyfile$comea<-sapply(file, function(x){
    return(sum(tab$ComEA[tab$file==x])>=1)
})
print("number of genomes with comr, roc or both")

print(table(tabbyfile$rocc, tabbyfile$comr))

print("without pneumophila")
print(table(tabbyfile$rocc[tabbyfile$species!="Legionella pneumophila"], tabbyfile$comr[tabbyfile$species!="Legionella pneumophila"]))


write.table(tabbyfile, args[5], row.names=FALSE, quote=FALSE, sep=";")


## Same with 1 line per species


species<-unique(tab$species)
tabbysp<-as.data.frame(species)

tabbysp$comr<-sapply(species, function(x){
    return(sum(tab$ComR[tab$species==x])>=1)
})


tabbysp$rocc<-sapply(species, function(x){
    return(sum(tab$RocC[tab$species==x])>=1)
})

tabbysp$comea<-sapply(species, function(x){
    return(sum(tab$ComEA[tab$species==x])>=1)
})
print("number of species with both genes")
print(table(tabbysp$rocc, tabbysp$comr))

# Seulement 6 espèces avec une séquence pour Roc et un sequenc pour comR????




#### Compare that with genome characteristics


tabbyfile$status<-ifelse((tabbyfile$comr & tabbyfile$rocc), "both", ifelse((tabbyfile$comr | tabbyfile$rocc), "one", "none"))

png("boxplots_prot.png")
par(mfrow=c(1,2))
boxplot(tabbyfile$size~tabbyfile$status)
boxplot(tabbyfile$ncontig~tabbyfile$status)
dev.off()





