#!/bin/bash
#$ -S /bin/bash
## name of the job to follow them
#$ -N blastprot
## name of the queue to be used
#$ -q E5-2670deb*,E5-2667v2*,E5-2667v4*
#$ -cwd
#$ -V
## where to put the log files (output and error) automatically generated by the cluster (different from the .log generated by DGINN)
## the dirs must exist before job is launched
#$ -o /home/mcariou/2020_Attaiech/pubgenomes
#$ -e /home/mcariou/2020_Attaiech/pubgenomes
 
### configurer l'environnement
#module purge
#module load Python/3.6.1 PhyML/3.0 Hyphy/2.3.14 

DATA="/home/mcariou/2020_Attaiech/"
OUT=$DATA"/pubgenomes"

mkdir -p $OUT

CONTIGTAB=$OUT"/tab_ncbi_contigs_parsed.csv"

TBLASTX_COMR=$DATA"/get_alignment/ComR_sup100.tblastx"
TBLASTX_ROCC=$DATA"/get_alignment/RocC_sup150.tblastx"
TBLASTX_COMEA=$DATA"/get_alignment/ComEA_sup80.tblastx"

OUTFILE=$OUT"/tab_ncbi_file_prot.csv"
Rscript --vanilla genomesVblastprot.R $TBLASTX_COMR $TBLASTX_ROCC $TBLASTX_COMEA $CONTIGTAB $OUTFILE > $OUT"/genomesVblastprot.Rout"




# fin
