## Material and methods

1038 assembled genomes from the legionella genus available on september 16th 2020 were extracted from the NCBI.
CDS were extracted from all genomes using the transdecoder.longorf script from TransDecoder (https://github.com/TransDecoder/TransDecoder/wiki), and were used to construct a blast database using the makeblastdb command [1]. 
Then, using as a query reference sequences from *Legionella pneumophila Paris* and *Legionella pneumophila Philadelphia* for the genes ComR, RocC and ComEA, potential orthologs were retrieved using blastall and blastdbcmd.
Alignment was performed with mafft [2] with default parameters.
In order to study the coevolution between ComR and RocC, we aimed at making a concatenate of ComR and RocC sequences.
As a negative control, we also made a concatenate of genes ComR and ComEA on one hand and RocC and ComEA on the other hand. Thus, only sequences from genomes containing all three genes were kept in the analysis.
In order to remove redundant sequences, we also filtered out sequences which were identical for all 3 genes. 
This automatic procedure resulted in alignments of 79 sequences. Manual curation suggested the removal of 4 slighly more divergent sequences. Both alignments (79 and 75 sequences) were included in the next ste of the analysis. 
Phylogeny infered from all three concatenates using phyML [3].
Alignment and phylogenies were submitted to the online tool Bis2Analyzer [4] (based on BIS2 [5,6]) for analysis of coevolving amino-acid pairs in protein sequences, and for the identification of residue networks. 

## Command lines

TransDecoder.LongOrfs -t \<fasta\> -m 100

makeblastdb -dbtype nucl -in \<input\> -hash_index -out \<output\> -parse_seqids

blastall -p tblastx -d \<db\> -i \<gene\>.fasta -e 0.0001  -m 8 -o \<gene\>.tblastx -K 1000

phyml -i \<aln\> -d aa



## References

- [1] Altschul, S.F, BLAST Algorithm. In eLS, John Wiley & Sons, Ltd (Ed.), 2014. [lien](https://onlinelibrary.wiley.com/doi/10.1002/9780470015902.a0005253.pub2)

- [2] Katoh S, MAFFT multiple sequence alignment software version 7: improvements in performance and usability, Molecular Biology and Evolution 30:772-780, 2013. [lien](https://academic.oup.com/mbe/article/30/4/772/1073398)

- [3] Guindon S, Dufayard J-F, Lefort V, Anisimova M, Hordijk W, Gascuel O, New Algorithms and Methods to Estimate Maximum-Likelihood Phylogenies: Assessing the Performance of PhyML 3.0. Systematic Biology, 59(3):307-21, 2010. [lien](https://academic.oup.com/sysbio/article/59/3/307/1702850)

- [4] Oteri F, Nadalin F, Champeimont R, Carbone A, BIS2Analyzer: a server for co-evolution analysis of conserved protein families, Nucleic Acids Research, 45(W1): W307–W314, 2017. [lien](https://academic.oup.com/nar/article/45/W1/W307/3787832)

- [5] Champeimont R, Laine E, Hu S-W, Penin F, Carbone A, Coevolution analysis of Hepatitis C virus genome to identify the structural and functional dependency network of viral proteins, Scientific reports, Nature Publishing Group, 6:26401, 2016. [lien](https://www.nature.com/articles/srep26401)

- [6] Dib L, Carbone A, Protein fragments: functional and structural roles of their coevolution networks, PLoS ONE, 7(11): e48124, 2012. [lien](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0048124)







