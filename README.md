# Coevolution between ComR and RocC in legionella

## 1. Objectives

Do ComR and RocC interact ? Do we observe signatures of coevolutions between proteic sequences that would suggest physical interactions between them.
Retrieve sequences of RocC and ComR in complete genomes of Legionella

## 2. Data and scripts

### 2.1. Sequences

**Gene sequences**
```
ls ~/2020_Attaiech/data/
data/ComEA.fasta  data/ComR.fasta  data/RocC.fasta  data/RocR.fasta
```

**Genomes**
```
# Genomes assembled by Christophe Ginevra
~/2020_Attaiech/data/Assemblies_112_FRM/

# Genomes available in NCBI
/Xnfs/ciridb/shared_data/Genomes/iGenomes/legionella/ncbi/ncbi-genomes-2020-09-16/
```

### 2.2. Git repository (scripts)

**Git repo**
~/2020_Attaiech/legionella_ppi 

Bash scripts are in the src/ repository except the R/knitr reports.

## 3. First manip (blastn)

### 3.1. Gene sequences retrieval

The script *2_evo_legio.sh* makes blats data bases on public legionella genomes and blasts ComR and RocC and retrieve corresponding sequences. 
The script *1_get_alignments.Rnw/pdf* sumarize this analyses.

**output:**
get_alignment

### 3.2. Check data

The script *2_explore_genomes.sh* (which calls *2_genomesVblast.R* and *2_explore_genomes_general.R*), gets genome size and number of contigs and which genomes contain whi
ch genes.

**outputs:**
pubgenomes/

Then the script *2_check_competence.sh* is used to look for the presence of ComEA and RocR in the complete genomes. Thes presence of ComEA ans RocR is required for competence to exist, then we require the presence of the 4 genes to use the data 


### 3.3. Alignment and phylogenies

*2_make_aln_and_tree.Rnw* reads the sequences outputs from evo_legio.sh and filter sequences in order to concatenate sequences from genomes where both genes are present. Then generate a aa fasta alignment and a phyML tree for BIS2

**output:**
phylogeny

### 3.4. Controle

*2_get_aln_controle.sh* : the aim is to make a negative control using sequence that are not supposed to interact. Here, the script retrieve ComEA sequences. *2_make_aln_and_tree.Rnw*  also make the aa aln and phylogeny for BIS2.


### 3.5. BIS2

We use Bis2Analyze online tool (BIS2 being difficult to run localy in command line)

2 run (12/2020):
-concatenate alignements of ComR and RocC for all genomes sharing both genes and once duplicated sequences removed.

-concatenate alignements of ComR and RocC for all genomes sharing both genes AND Comea and RocR, and once duplicated sequences removed.

1 run ComR-COmEA:
- in order to check if we still find clusters of coevolution between proteins that are not supposed to coevolve.
(might not be a good example?)

1 run ComEA-RocC:
- Better controm because ComEAand RocC are not supposed to interact

## 4. Proteic alignement

### 4.1. Sequence retrieval

Retrieve sequences based on proteic sequences. It might allow to retrieve more sequences and also make the alignement step easier.

*3_blastprot_legio.sh*

I retrieve alignements for ComR, RocC and ComEA.

```
mcariou@e5-2670comp1:~/2020_Attaiech/legionella_ppi$ ls -lh ../get_alignment/*prot_aln.fasta
-rw-r--r-- 1 mcariou ciri 2.3M Jan 27 19:27 ../get_alignment/ComEA_homologs_prot_aln.fasta
-rw-r--r-- 1 mcariou ciri 6.3M Jan 27 19:26 ../get_alignment/ComR_homologs_prot_aln.fasta
-rw-r--r-- 1 mcariou ciri 2.7M Jan 27 19:26 ../get_alignment/RocC_homologs_prot_aln.fasta
```

Using a first stringent threshold for hit length a retrieved fewer species. Searching in the original blast, I observed that several genomes where excluded by this species.

Aftercheck on row blast I observed that many hit were lost because by this threshold. I used lower threshold to retrieve more sequences:

-ComR > 100 aa

-RocR > 150 aa

-ComEA > 80 aa


```
3_blastprot_explore_genome.sh
::::::::::::::
/home/mcariou/2020_Attaiech/pubgenomes/genomesVblast1.Rout
::::::::::::::
       
        FALSE  TRUE
  FALSE 54390   744
  TRUE    755   131
[1] "number of genomes with comr, roc or both"
       
        FALSE TRUE
  FALSE   127   36
  TRUE     25  850
[1] "without pneumophila"
       
        FALSE TRUE
  FALSE   126   36
  TRUE     25   12
[1] "number of species with both genes"
       
        FALSE TRUE
  FALSE    30   12
  TRUE      6    6
null device 
          1 
::::::::::::::
/home/mcariou/2020_Attaiech/pubgenomes/genomesVblastprot.Rout
::::::::::::::
[1] "number of genomes with comr, roc or both"
       
        FALSE TRUE
  FALSE    52   96
  TRUE      6  884
[1] "without pneumophila"
       
        FALSE TRUE
  FALSE    51   96
  TRUE      6   46
[1] "number of species with both genes"
       
        FALSE TRUE
  FALSE    11   23
  TRUE      3   17
null device 
          1 
```

### 4.2. Filter alignement

```
3_make_aln_and_trees_from_tblastx.Rnw
```

To do:

Filter alignement to keep only genome with the 3 genes.	

### 4.3. BIS2.

Bis2 analyzer run 3x3, on each pairs of genes (ComR-RocC, ComR-ComEA, RocC-ComEA).
The first triplet of alignments consists on all retrieved sequences.
The second triplet of alignments consists on longest sequences (exclusion of most non-pneumophila sequences)
The third triplet consists in shortened sequences (to exclude positions with missing data)

## 5. Retrieval of complete protein sequences

### 5.1. Objective

Discussion with Leatitia Attaiech. 
There is a problem with alignments used. The incomplete sequences correspond to the end of the hit not to the end of the protein. It will be useful to retrieve the complete sequences.No matter if they are too divergent.

1 approach: Make a blast database based on the cds, and then retrieve the complete sequence. This require to annotate each genomes using [TransDecoder](https://github.com/TransDecoder/TransDecoder)

### 5.2. Make a new database.

**Annotation Transdecoder**

```
4_Transdecoder.sh
```
This script apply transdecoder.longorf on all genome. Then make blast databases.


**Output**

```
~/2020_Attaiech/prot_db/Transdecoder/
```

**Make databases**

```
~/4_make_protdb.sh

```
Concatenate all cds. Then split in 35-36 files and makes blastdb.

**Output**

```
~/2020_Attaiech/prot_db/blastbd/
```

### 5.3. Blasts

```
4_blast_legio.sh
```
Extract complete CDS, not only the mapping region.

**Output**

```
~/2020_Attaiech/get_alignment_cds/
```

### 5.4. Explore Genomes

```
4_explore_genomes.sh

# calls

4_genomesVblastprot.R
```

**Output**

```
~/2020_Attaiech/pubgenomes_cds/
```

### 5.5. Make concatenate and phylogeny

see the knitr report 4\_make\_aln\_and\_trees\_from\_cds.pdf, from the Rnw script:
```
~/2020_Attaiech/legionella_ppi/4_make_aln_and_trees_from_cds.Rnw
``` 

### 5.5. Corrections

**Bug?**

I wanted to check why the gene retrieval on CDS yielded much less sequences than the blast on complete genomes. The diminution of sequences occur during the suppression of identical sequences.

Never the less, I checked if the script 4_blast_legio.sh, retrieve the expected nomber of sequences.

```
# number of unique results blast hits on CDS (=sequences to be retrieved)
cat ../get_alignment_cds/ComR_sup100.tblastx | awk '{print $2}'  | sort | uniq | wc -l
1098
# number of retrieved sequences (by blastdbcmd)
mcariou@e5-2670comp1:~/2020_Attaiech/legionella_ppi$ grep ">" ../get_alignment_cds/ComR_homologs_prot.fasta | awk '{print $1}'  | sort | uniq | sed 's/>//g' | wc -l
1087
```
I couldn't understand why 11 sequences were consistantly not retrieved. Individual retrieval works fine. So I made a "rescue " retrieval: 4_blast_legio_recup.sh. Here are the number of rescued sequences for each gene:

```
tail -15 ~/2020_Attaiech/get_alignment_cds/recup.o203707 
ComR
11
sequences uniques
RocC
28
sequences uniques
ComEA
11
sequences uniques
```
The number of retrieved sequences doesn't change muche so it shouldn't change the results much, but it is still cleaner to have all sequences.

**Check on the duplicate filtration step**

The stage were a lot of sequences are lost is the filtration of identical sequences. That is, when 2 genomes A and B had identical sequences for the 3 genes (ComR, RocC and ComEA), I kept only one sequence (random choice).

To check on that, I calculated the number of haplotypes (= unique sequences) for each gene, on the alignment before filtration. I expected less haplotype, than when I ask for an identical sequences for all 3 genes, but this tests allows to check that I didn't remove a large number of sequences by mistake.

```
> #install.packages("haplotypes")
> library(haplotypes)
> #read your fasta file
> x<-read.fas("comr_nt_tblastx_V0.fasta")
> #this is your sequences
> #as.matrix(x)
> 
> #h$infer haplotypes
> h<-haplotypes::haplotype(x,indels="m")
> # length(h) gives the total number of haplotypes #
> length(h)
[1] 31
> 
> x<-read.fas("rocc_nt_tblastx_V0.fasta")
> h<-haplotypes::haplotype(x,indels="m")
> length(h)
[1] 36
> 
> x<-read.fas("comea_nt_tblastx_V0.fasta")
> h<-haplotypes::haplotype(x,indels="m")
> length(h)
[1] 27
```

This confirms that we don't retrieve a lot of different sequences.

**New Run**

The alignement was done using the same script as previouly. It contains 79 sequences. I ran BIS2 on this nex alignment. Then, following LA suggestions, I removed the divergent sequences which forced the additions of "X" in the alignement.

Rem:
During the duplicate filtration, the sequence kept depends on the initial sequence order and varied a little.I checke that the 4 sequences removed were the same as in the previous alignment (even if they don't have the same name).



## 6. Presence/Absence of genes (manip 2022)

The script 5\competence\_in\_genomes sumarize the results of the screening of the presence of ComEA, ComR and RoC in legionelle genomes.
It also parse results from the PSIblast search of the genes in the phyloref database (database designed for species phylogenetic analysis in Legionelle).

Alignment from this pipeline are in the repertory *aln_phyloref*


